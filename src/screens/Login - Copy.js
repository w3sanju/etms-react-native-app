import React, {Component} from 'react';
import {View, Button,Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet} from 'react-native';

import COLOR from '../styles/Color';
import COLOR_SCHEME from '../styles/ColorScheme';
import styles from '../styles/Styles';

import Gotham from '../assets/GothamCity.jpg';

export default class Login extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
          <View style={ styles.container }>
            <Image source={Gotham} style={styles.backgroundImage}/>
              <View style={ styles.loginForm }>
                <View style={ styles.loginContainer }>
                    <Text style={styles.h1}>TEST</Text>
                    <Text style={styles.title}>Goto Detail Page</Text>
                    <Button title={"Home page"} onPress={()=>this.props.navigation.navigate('Home')} />
                </View>
              </View>
          </View>
        )
    }
};
