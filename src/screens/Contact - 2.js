import React, {Component} from 'react';
import {View, Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
// import { Button, Paragraph, Dialog, Portal } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph, Divider, Dialog, Portal, Provider, Caption } from 'react-native-paper';

import COLOR from '../styles/Color';
import COLOR_SCHEME from '../styles/ColorScheme';
// import styles from '../styles/Styles';

// import Gotham from '../assets/GothamCity.jpg';

const rows = [
  { id: 0, title: 'View' },
  { id: 1, title: 'Text' },
  { id: 2, title: 'Image' },
  { id: 3, title: 'ScrollView' },
  { id: 4, title: 'ListView' },
]


function Item({ id, title, selected, onSelect }) {
  return (
    // <TouchableOpacity
    //   onPress={() => onSelect(id)}
    //    style={styles.listItems}
    // >
    //   <Text style={styles.title}>{title}</Text>
    // </TouchableOpacity>
    <Text style={styles.title}>{title}</Text>
  );
}

export default class Contact extends Component {
    constructor(props) {
        super(props);
    }


    state = {
      visible: false,
    };

    showDialog = () => this.setState({ visible: true });

    hideDialog = () => this.setState({ visible: false });


    // <Button title={">>"} onPress={() => this.props.navigation.navigate('Home')} />

    render() {
        return (
          <SafeAreaView style={{
        flex: 1,
      }}>
      <Provider>
        <View style={{ flex: 1 }}>
        <ImageBackground
            style={{
              flex: 1,
              resizeMode: 'cover',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
          }}
          source={{ uri: 'https://picsum.photos/500/800' }}>
            <ScrollView style={styles.scrollViewStyle}>

                <Card>
                  <Card.Title title="Card Title" subtitle="Card Subtitle" left={(props) => <Avatar.Icon {...props} icon="folder" />} />
                  <Card.Content>
                  </Card.Content>
                  <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
                  <Card.Actions>
                    <Button icon="home" mode="contained" onPress={() => this.props.navigation.navigate('Home')}>Home</Button>
                    <Button onPress={this.showDialog}>Dialog</Button>
                  </Card.Actions>
                </Card>

      <Portal>
      <Dialog
      visible={this.state.visible}
      onDismiss={this._hideDialog} style={{
                  borderTopRightRadius: 15, borderTopLeftRadius: 15, backgroundColor: '#fff', margin:0, overflow: 'hidden'}}>
      <Dialog.Title style={{
        margin:0,
                  borderTopLeftRadius: 15,
                  borderTopRightRadius: 15, backgroundColor:'#6200ee', paddingVertical:8, color:'white', fontSize:14, textAlign:'center'}}>Check out Cab
                  <Button icon="home" mode="contained" onPress={this.hideDialog} style={{ flex:1, position:'absolute', right:0, top:0, borderTopRightRadius: 15}}>X</Button>

                  </Dialog.Title>
      <Dialog.ScrollArea style={{padding:0}}>
                  <ScrollView style={{ marginTop:-11, paddingVertical:10 }} contentContainerStyle={{   }}>
                  <Image style={{
                    flex: 1,
          resizeMode: 'cover',
          width:'100%', height:120, }} source={{ uri: 'https://picsum.photos/500/800' }}/>

                    <FlatList style={{ flex: 1, margin:0,paddingHorizontal:20, marginHorizontal:0, marginTop:5, paddingVertical:15 }}
                      data={rows}
                      renderItem={({ item, index }) => (
                        <View style={styles.listItems,{ flex:1, paddingVertical:5,paddingHorizontal:0 }}
                          id={item.id}
                        >
                        <Text style={styles.title,{color:'#000'}}>{index+1}}
                        <Text style={styles.title,{color:'#409fff'}}> {item.title}</Text></Text></View>
                      )}
                      numColumns={2}
                      keyExtractor={item => item.id}
                    />
                  <View style={{marginHorizontal:20, flex:1, marginBottom:10, }}>
                  <Caption style={{fontSize:12, color:'#4861fb', fontWeight:'bold'}}>Cab No:</Caption>
                    <Paragraph style={{borderWidth: 0, borderLeftWidth: 4, marginHorizontal:0, padding:10, borderTopLeftRadius: 5,
                  borderBottomLeftRadius: 5, borderColor:'#6200ee', backgroundColor:'#f2f2f2', fontSize:12,}}>
                  This is a scrollable area. Lorem</Paragraph>
                  </View>
                  </ScrollView>
                </Dialog.ScrollArea>
        <Dialog.Actions>
          <Button color={'#000'} style={{flex:1,}} onPress={this.hideDialog}>close</Button>
        </Dialog.Actions>
    </Dialog>
        </Portal>

          </ScrollView>
          </ImageBackground>
        </View>
        </Provider>
      </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: '100%',
    position:'relative',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    // flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: 'grey',
    // height: 500
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
    // flex:2,
  },
  bgImage: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'green',
    // justifyContent: 'center',
    // alignItems: 'center',
    // flexDirection: 'column',
    // height: 100
  },
  scrollViewStyle: {
    // position: 'relative',
    // paddingTop: 10,
    marginTop: 0,
    flex: 1,
    padding:10
    // height: 300
  },
  loginContainer: {
    flex: 1,
    width: '100%',
    padding:20,
    // height: 250,
    // textAlign: 'center',
    //alignItems: 'center',
    // backgroundColor: 'orange',
    alignSelf: 'center',

    // flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',

    backgroundColor: 'powderblue',
    // height: '50%',
    // display: 'none'
  },
  loginbutton: {
    // flex: 2,
    color: 'white',
    fontSize: 16,
    // alignSelf: 'flex-end',
    paddingTop: 20,
    textAlign: 'center',
    backgroundColor:'skyblue'
    //justifyContent: 'space-between',
    //alignItems: 'flex-end',
  },
  loginForm: {
    // position: 'absolute',
    // top: 0,
    // bottom: 0,
    // left: 0,
    // right: 0,
    // margin: 20,
    flex:2,
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: 'skyblue',
    height: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 17,
    marginHorizontal: 0,
    marginBottom: 20,
    paddingHorizontal: 0,
    // backgroundColor: '#4D243D'
    // fontWeight: 'bold',
    // color: COLOR.WHITE
  },
  listItems: {
    //flexDirection: 'column',
    // backgroundColor: '#4D243D',
    // alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    // marginHorizontal: 10,
    marginVertical: 8,
    paddingHorizontal: 15,
    // backgroundColor: '#409fff',
    fontSize:14,
    textAlign:'left',
     paddingVertical: 8, color:'white'
  }
})
