import React, {Component} from 'react';
import {View, Button,Text, Image,
ImageBackground, SafeAreaView,ScrollView} from 'react-native';

import COLOR from '../styles/Color';
import COLOR_SCHEME from '../styles/ColorScheme';
import styles from '../styles/Styles';

import Gotham from '../assets/GothamCity.jpg';

export default class Home extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
          <SafeAreaView>
            <ScrollView>
              <View style={styles.container}>
                  <Image source={Gotham} style={styles.backgroundImage} resizeMode="contain"/>
                  <View style={styles.containerAbsolute, {marginBottom: 20}}>
                    <Text style={[styles.h1, styles.margin]}>Home</Text>
                    <Text style={styles.title}>Goto Detail Page</Text>
                    <Button style={styles.aligncenter} title={"Login page"} onPress={()=>this.props.navigation.navigate('Login')} />
                  </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        )
    }
}
