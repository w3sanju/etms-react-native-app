import React, { Component } from 'react'
import { Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const styles = {
  container: {
    flex: 1,
    position:'relative',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
    // flex:2,
  },
  bgImage: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'green',
    // justifyContent: 'center',
    // alignItems: 'center',
    // flexDirection: 'column',
    // height: 100
  },
  scrollViewStyle: {
    // position: 'relative',
    // paddingTop: 10,
    marginTop: 0,
    flex: 1,
    padding:10
    // height: 300
  },
  loginContainer: {
    flex: 1,
    width: '100%',
    padding:20,
    // height: 250,
    // textAlign: 'center',
    //alignItems: 'center',
    // backgroundColor: 'orange',
    alignSelf: 'center',

    // flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',

    backgroundColor: 'powderblue',
    // height: '50%',
    // display: 'none'
  },
  loginbutton: {
    // flex: 2,
    color: 'white',
    fontSize: 16,
    // alignSelf: 'flex-end',
    paddingTop: 20,
    textAlign: 'center',
    backgroundColor:'skyblue'
    //justifyContent: 'space-between',
    //alignItems: 'flex-end',
  },
  loginForm: {
    // position: 'absolute',
    // top: 0,
    // bottom: 0,
    // left: 0,
    // right: 0,
    // margin: 20,
    flex:2,
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: 'skyblue',
    height: '100%',
    alignSelf: 'center',
  },
  dialogContainer:{
    borderTopRightRadius: 10, borderTopLeftRadius: 10,  margin:0,
    overflow: 'hidden',
    paddingVertical:0,
    // flex:1,
    // flexDirection: 'column',
  },
  dialogCloseButton:{
    // flex:1,
    position:'absolute', right:-10, top:0, borderTopRightRadius: 15, padding:0,
  },
  dialogCaption:{fontSize:12, color:'#4861fb', fontWeight:'bold'},
  dialogTitle:{  backgroundColor:'#6200ee', paddingVertical:8, color:'white', fontSize:19,  textAlign:'center' , marginHorizontal:0, marginTop:0, marginBottom:0,
                  height:45, width:'100%',
                  fontFamily: 'sans-serif-thin',
                  fontWeight:'100' },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    fontSize: 17,
    marginHorizontal: 0,
    marginBottom: 20,
    paddingHorizontal: 0,
    // backgroundColor: '#4D243D'
    // fontWeight: 'bold',
    // color: COLOR.WHITE
  },
  dialogCoverImage:{width:'100%', height:100},
  dialogFlatlist:{flex: 1, height:'100%', margin:0,paddingHorizontal:20, marginHorizontal:0, marginTop:5, paddingVertical:15},
  dialogParagraph:{borderWidth: 0, borderLeftWidth: 4, marginHorizontal:0, padding:10, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderColor:'#6200ee', backgroundColor:'#f2f2f2', fontSize:12, paddingLeft:15},
  dialogButton:{flex:1, },
  dialogLabel:{fontSize: 12,
    marginHorizontal: 0,
    paddingVertical: 2,  }
}
export default styles;
﻿
