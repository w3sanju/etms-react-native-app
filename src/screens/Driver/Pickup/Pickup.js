import React, {Component} from 'react';
import { View, Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet, FlatList, TouchableOpacity, BackHandler, Alert } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph, Divider, Dialog, Portal, Provider, Caption, ActivityIndicator, Colors, ProgressBar, Snackbar } from 'react-native-paper';

import COLOR from '../../styles/Color';
import COLOR_SCHEME from '../../styles/ColorScheme';
import styles from './PickupStyle'

import checkoutBanner from '../../assets/checkOutBanner.png';

import Utils from '../../controller/Utils';
import DialogBox from '../../component/DialogBox'

const rows = [
  { id: 0, title: 'View' },
  { id: 1, title: 'Text' },
  { id: 2, title: 'Image' },
  { id: 3, title: 'ScrollView' },
  { id: 4, title: 'ListView' },
]


function Item({ id, title, selected, onSelect }) {
  return (
    // <TouchableOpacity
    //   onPress={() => onSelect(id)}
    //    style={styles.listItems}
    // >
    //   <Text style={styles.title}>{title}</Text>
    // </TouchableOpacity>
    <Text style={styles.title}>{title}</Text>
  );
}

export default class Pickup extends Component {
    constructor(props) {
        super(props);
    }


    state = {
      visible: false,
      isLoading: true,
      snackbarVisible: false,
      myMvc: Utils.guid()
    };

    showDialog = () => this.setState({ visible: true });

    hideDialog = () => this.setState({ visible: false });




    componentDidMount() {
        // this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
        //   if(this.state.visible)
        //   {
        //     Alert.alert("Dialog", "Are you sure you want to close the Dialog", [
        //       { text: "Cancel", onPress: () => {}, style: "cancel" }, { text: "Close", onPress: () => this.handleLogout() }], { cancelable: false });
        //   }
        //   else
        //   {
        //     Alert.alert("Exit", "Are you sure you want to logout?", [
        //       { text: "Cancel", onPress: () => {}, style: "cancel" }, { text: "Close", onPress: () => this.handleLogout() }], { cancelable: false });
        //   }
        //     return true;
        // });

        return fetch('https://facebook.github.io/react-native/movies.json')
          .then((response) => response.json())
          .then((responseJson) => {

            this.setState({
              isLoading: false,
              dataSource: responseJson.movies,
            }, function(){

            });

          })
          .catch((error) =>{
            console.error(error);
          });
    }

    // <Button title={">>"} onPress={() => this.props.navigation.navigate('Home')} />

    render() {

      // const UserModel = this.props.navigation.getParam('userData');
      //
      //   console.log("Username", UserModel.username)


    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator animating={true} color={Colors.red800} size={'large'} />
        </View>
      )
    }

        return (
          <SafeAreaView style={{
        flex: 1,
      }}>
      <Provider>
        <View style={{ flex: 1 }}>
        <ImageBackground
            style={{
              flex: 1,
              resizeMode: 'cover',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
          }}
          source={{ uri: 'https://picsum.photos/500/800' }}>
            <ScrollView style={styles.scrollViewStyle}>

                <Card>
                  <Card.Title title="Card Title" subtitle="Card Subtitle" left={(props) => <Avatar.Icon {...props} icon="folder" />} />
                  <Card.Content>
                  </Card.Content>
                  <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
                  <Card.Actions>
                    <Button icon="home" mode="contained" onPress={() => this.props.navigation.navigate('Home')}>Home</Button>
                    <Button onPress={this.showDialog}>Dialog</Button>
                  </Card.Actions>
                </Card>


      <Portal>
      <Dialog visible={this.state.visible}
             onDismiss={this.hideDialog} style={styles.dialogContainer}>
            <Dialog.Title style={styles.dialogTitle}>Check out Cab</Dialog.Title>

                  <Button mode="text" color={'#fff'} onPress={this.hideDialog} style={styles.dialogCloseButton}><Text style={{fontSize:18, fontFamily: 'sans-serif-thin'}}>X</Text></Button>
            <Dialog.ScrollArea style={{  marginHorizontal:-25, }}>
              <ScrollView>
                <Image style={styles.dialogCoverImage} source={checkoutBanner}/>
                <Text>{this.state.myMvc}</Text>
                  <FlatList style={styles.dialogFlatlist}
                      data={this.state.dataSource}
                      renderItem={({ item, index }) => (
                        <View style={{ flex:1, paddingVertical:5, }}
                          id={item.id}
                        >
                        <Text style={styles.title,{color:'#000'}}>{index+1})
                        <Text style={styles.title,{color:'#409fff'}}> {item.title} </Text></Text></View>
                      )}
                      numColumns={2}
                      keyExtractor={item => item.id}
                    />
                    <View style={{marginHorizontal:20, marginBottom:20 }}>
                      <Caption style={styles.dialogCaption}>Cab No:</Caption>
                      <View style={styles.dialogParagraph}>
                        <Text style={styles.dialogLabel}>Vehicle Number : KL07</Text>
                        <Text style={styles.dialogLabel}>Vehicle Details : Lorem</Text>
                      </View>

                    </View>
                </ScrollView>
            </Dialog.ScrollArea>
            <Dialog.Actions>
              <Button color={'#000'} style={styles.dialogButton} onPress={this.hideDialog}><Text style={{flex:1, textTransform: 'capitalize',}}>Close</Text></Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>

          </ScrollView>
          </ImageBackground>
        </View>
        </Provider>
      </SafeAreaView>
        )
    }
}
