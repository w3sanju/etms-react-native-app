import React, {Component} from 'react';
import {View, Button,Text, Image, Alert, ImageBackground, SafeAreaView,ScrollView, TextInput, TouchableOpacity, FlatList } from 'react-native';

import { Provider } from 'react-native-paper';

import COLOR from '../../styles/Color';
import COLOR_SCHEME from '../../styles/ColorScheme';
import styles from '../../styles/Styles';

import Gotham from '../../assets/GothamCity.jpg';

import Box from '../../controller/Box';

import DialogBox from '../../component/DialogBox'

// import * as actions from '../../controller/Apimaster';
// import UserModel, {getUsefulContents} from '../../controller/Apimaster';

import * as fromAuth from '../../controller/Apimaster';

import ApiModel from '../../model/ApiModel';

export default class Home extends Component {
    constructor(props) {
        super(props);
        // this.monkey = 'no monkey'

        this.state = {
          monkey: 'no monkey',
          myUser: '',
          dataSource: null,
          users:[],
          fetchResultValid: false
        }

    }

    handlerApiCall = () => {

      var a = ApiModel.getApi('https://jsonplaceholder.typicode.com/users');
      // a.then((result) => alert(result.length) );
      a.then(
        (result) => {
        if(typeof result === 'object')
        {
          this.setState({users: result, fetchResultValid: true})
        }
        else {
          this.setState({fetchResultValid: false})
          alert(typeof result)
        }
      }
        // this.setState({users: result})
      );

      // ApiModel.getApi();
      // this.setState({monkey: ApiModel.getApi()})

      // ApiModel.getApi(this.userId).then(monkey => {
      //
      //           this.setState({monkey});
      //
      //
      //   // const properties = JSON.parse(monkey);
      //   // const properties = monkey;
      //   //
      //   // if(properties!== null && typeof properties === 'object')
      //   // {
      //   // 	alert("It's a valid Json QR data");
      //   // }
      //   // else
      //   // {
      //   // 	alert("It's Not a valid Json QR data");
      //   // }
      //
      // });

    };


    handlerSimpleCall = () => {
      // this.monkey = 'handlerSimpleCall';
      this.setState({monkey: 'handlerSimpleCall'})
    //Calling a function of other class (without arguments)
    // alert(UserModel.myFunction);
    // getUsefulContents();
    fromAuth.functionA();
  };

  // myHandlerSimpleCall = () => {
  //   this.monkey = 'myHandlerSimpleCall';
  //   //Calling a function of other class (without arguments)
  //   alert(UserModel.myFunction());
  // };
  //
  //   callWithoutArgument = () => {
  //       //Calling a function of other class (without arguments)
  //        UserModel.functionWithoutArg();
  //     };

                // const me = new UserModel('Robin', 'Wieruch');
                // this.monkey = UserModel.getName();


    handlePress = async () => {
      // this.monkey = this.child.getName();       //<--- Calling child class method
            // DialogBox.username = 'w3sanju';
            // UserModel.HelloChandu();

            // this.props.navigation.navigate('Pickup',{userData: UserModel});

              // alert(me.getName());
              // this.monkey = me.getName();
            }

// mylogin = () => alert(' user successfully logged in ');

    // mylogin = () => {
    //   alert(' user successfully logged in ');
    // }

  mylogin = () => {
    // HelloChandu();
    // Foo.handleClick();
    justAnAlert();

    // this.login('email','password').then(() => {
    //   alert(' user successfully logged in ');
    // })
  };


              // <DialogBox greeting={greeting}/>

              // <Text style={{color:'black'}}>{this.state.users}</Text>

    render() {

const greeting = 'Welcome to React';

const { users, fetchResultValid } = this.state


        return (
          <Provider>
          <SafeAreaView>
            <ScrollView>
            <View style={{alignItems:'center', flex:1, justifyContent: 'center', flexDirection: 'column', padding: 20}}>
            <Button style={{flex:1, width: '100%'}} title="Get ApiCall" color="#0059B2" onPress={this.handlerApiCall}>Get ApiCall</Button>

            { !fetchResultValid &&
              <View style={{ width: '100%', textAlign: 'center', paddingVertical: 15 }}>
                <Text style={{textAlign: 'center'}}>InValid Result format!</Text>
              </View>
            }

            { fetchResultValid && users &&
                this.state.users.map((p, index) => {
                    return <View style={{ borderBottomWidth: 1, borderColor:'#000', width: '100%', textAlign: 'center', paddingVertical: 10 }} key={index}>
                      <Text style={{textAlign: 'center'}}>{index+1} - {p.username}</Text>
                    </View>;
                })
            }
            </View>
            </ScrollView>
          </SafeAreaView>
          </Provider>
        )
    }
}
