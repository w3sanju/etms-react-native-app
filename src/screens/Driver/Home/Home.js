import React, {Component} from 'react';
import {View, Button,Text, Image, Alert, ImageBackground, SafeAreaView,ScrollView, TextInput, TouchableOpacity, FlatList } from 'react-native';

import { Provider } from 'react-native-paper';

import COLOR from '../../styles/Color';
import COLOR_SCHEME from '../../styles/ColorScheme';
import styles from '../../styles/Styles';

import Gotham from '../../assets/GothamCity.jpg';

import Box from '../../controller/Box';

import DialogBox from '../../component/DialogBox'

// import * as actions from '../../controller/Apimaster';
// import UserModel, {getUsefulContents} from '../../controller/Apimaster';

import * as fromAuth from '../../controller/Apimaster';

import ApiModel from '../../model/ApiModel';

export default class Home extends Component {
    constructor(props) {
        super(props);
        // this.monkey = 'no monkey'

        this.state = {
          monkey: 'no monkey',
          myUser: '',
          dataSource: null,
          users:[],
          fetchResultValid: false,
          validationMsg: ''
        }

    }

    componentDidMount(){
      this.handlerApiCall();
    }

    handlerApiCall = () => {

      var a = ApiModel.getApi('https://jsonplaceholder.typicode.com/users', 'username');
      // a.then((result) => alert(result.length) );
      a.then(
        (result) => {

          // if(typeof result === 'object' && result.length !== null)
          // {
          //   if(result.hasOwnProperty('username'))
          //   {
          //     this.setState({users: result, fetchResultValid: true})
          //   }
          //   else
          //   {
          //     this.setState({fetchResultValid: false, validationMsg: 'Has no OwnProperty'})
          //   }
          // }
          // else if(typeof result === 'string' && result.length !== null)
          // {
          //   this.setState({fetchResultValid: false, validationMsg: result})
          // }
          // else {
          //   this.setState({fetchResultValid: false, validationMsg: 'InValid Result format !'})
          // }


          if(typeof result === 'object' && result.length !== null)
          {

            alert(result.hasOwnProperty('username'))
            this.setState({users: result, fetchResultValid: true})
          }
          else {
            this.setState({fetchResultValid: false})
          }
        }
      );


      // ApiModel.getApi(this.userId).then(monkey => {
      //
      //           this.setState({monkey});
      //
      //
      //   // const properties = JSON.parse(monkey);
      //   // const properties = monkey;
      //   //
      //   // if(properties!== null && typeof properties === 'object')
      //   // {
      //   // 	alert("It's a valid Json QR data");
      //   // }
      //   // else
      //   // {
      //   // 	alert("It's Not a valid Json QR data");
      //   // }
      //
      // });

    };


    handlerSimpleCall = () => {
      this.setState({monkey: 'handlerSimpleCall'})
    //Calling a function of other class (without arguments)
    // alert(UserModel.myFunction);
    // getUsefulContents();
    fromAuth.functionA();
  };



              // <DialogBox greeting={greeting}/>

              // <Text style={{color:'black'}}>{this.state.users}</Text>

    render() {

const greeting = 'Welcome to React';

const greetingArray = this.state.users

const { users, fetchResultValid, validationMsg } = this.state


        return (
          <Provider>
          <SafeAreaView>
            <ScrollView>

                        <DialogBox greeting={greeting} greetingArray={this.state.users}/>

            <View style={styles.container}>
            <Button style={styles.aligncenter, {flex:1, width: '100%', marginBottom:25}} title="Get ApiCall" color="#0059B2" onPress={this.handlerApiCall}>Get ApiCall</Button>

            { !fetchResultValid && validationMsg !='' &&
              <View style={{ width: '100%', textAlign: 'center', paddingVertical: 15 }}>
                <Text style={{textAlign: 'center'}}>{this.state.validationMsg}</Text>
              </View>
            }

            { fetchResultValid && users &&
                this.state.users.map((p, index) => {
                    return <View style={{ borderBottomWidth: 1, borderColor:'#000', width: '100%', textAlign: 'center', paddingVertical: 10 }} key={index}>
                      <Text style={{textAlign: 'center'}}>{index+1} - {p.username}</Text>
                    </View>;
                })
            }
            <Button style={styles.aligncenter} title={"Login page"} onPress={()=>this.props.navigation.navigate('Pickup')} />

            </View>
            </ScrollView>
          </SafeAreaView>
          </Provider>
        )
    }
}
