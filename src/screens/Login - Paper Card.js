import React, {Component} from 'react';
import {View, Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
// import { Button, Paragraph, Dialog, Portal } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph, Divider, Dialog, Portal, Provider } from 'react-native-paper';

import COLOR from '../styles/Color';
import COLOR_SCHEME from '../styles/ColorScheme';
// import styles from '../styles/Styles';

// import Gotham from '../assets/GothamCity.jpg';

const rows = [
  { id: 0, title: 'View' },
  { id: 1, title: 'Text' },
  { id: 2, title: 'Image' },
  { id: 3, title: 'ScrollView' },
  { id: 4, title: 'ListView' },
]

function Item({ id, title, selected, onSelect }) {
  return (
    <TouchableOpacity
      onPress={() => onSelect(id)}
      style={[
        styles.item,
        { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
      ]}
    >
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
}

export default class Login extends Component {
    constructor(props) {
        super(props);
    }


    state = {
      visible: false,
    };

    showDialog = () => this.setState({ visible: true });

    hideDialog = () => this.setState({ visible: false });

    // <Button title={">>"} onPress={() => this.props.navigation.navigate('Home')} />

    render() {
        return (
          <SafeAreaView style={{
        flex: 1,
      }}>
      <Provider>
        <View style={{ flex: 1 }}>
        <ImageBackground
            style={{
              flex: 1,
              resizeMode: 'cover',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
          }}
          source={{ uri: 'https://picsum.photos/500/800' }}>
            <ScrollView style={styles.scrollViewStyle}>
              <View style={{
                margin:10,
                textAlign: 'center', backgroundColor: 'white', borderRadius: 5, padding: 10
              }}>
                <Text style={styles.title}>Goto Home Page</Text>
                <Button icon="home" mode="contained" onPress={() => this.props.navigation.navigate('Home')}>Home >></Button>

                <Divider />

                <Card>
    <Card.Title title="Card Title" subtitle="Card Subtitle" left={(props) => <Avatar.Icon {...props} icon="folder" />} />
    <Card.Content>
      <Title>Card title</Title>
      <Paragraph>Card content</Paragraph>
    </Card.Content>
    <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
    <Card.Actions>
      <Button>Cancel</Button>
      <Button onPress={this.showDialog}>Ok</Button>
    </Card.Actions>
  </Card>

  <Portal>
          <Dialog
             visible={this.state.visible}
             onDismiss={this.hideDialog}>
            <Dialog.Title>Alert</Dialog.Title>
            <Dialog.Content>
              <Dialog.ScrollArea>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 24 }}>
                <FlatList
                data={rows}
                renderItem={({ item, index }) => (
                  <Item style={styles.item} style={{marginVertical: 8, padding:10, backgroundColor:'#409fff', color:'white'}}
                    id={item.id}
                    title={index+1 + item.title}
                  />
                )}
                numColumns={2}
                keyExtractor={item => item.id}
              />
                  <Paragraph>This is a scrollable area</Paragraph>
                </ScrollView>
              </Dialog.ScrollArea>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={this.hideDialog}>Done</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>

              </View>
          </ScrollView>
          </ImageBackground>
        </View>
        </Provider>
      </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: '100%',
    position:'relative',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    // flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: 'grey',
    // height: 500
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
    // flex:2,
  },
  bgImage: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'green',
    // justifyContent: 'center',
    // alignItems: 'center',
    // flexDirection: 'column',
    // height: 100
  },
  scrollViewStyle: {
    // position: 'relative',
    // paddingTop: 10,
    marginTop: 0,
    flex: 1,
    // height: 300
  },
  loginContainer: {
    flex: 1,
    width: '100%',
    padding:20,
    // height: 250,
    // textAlign: 'center',
    //alignItems: 'center',
    // backgroundColor: 'orange',
    alignSelf: 'center',

    // flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',

    backgroundColor: 'powderblue',
    // height: '50%',
    // display: 'none'
  },
  loginbutton: {
    // flex: 2,
    color: 'white',
    fontSize: 16,
    // alignSelf: 'flex-end',
    paddingTop: 20,
    textAlign: 'center',
    backgroundColor:'skyblue'
    //justifyContent: 'space-between',
    //alignItems: 'flex-end',
  },
  loginForm: {
    // position: 'absolute',
    // top: 0,
    // bottom: 0,
    // left: 0,
    // right: 0,
    // margin: 20,
    flex:2,
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: 'skyblue',
    height: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 19,
    marginBottom: 20
    // fontWeight: 'bold',
    // color: COLOR.WHITE
  },
  listItems: {
  //flexDirection: 'column',
    backgroundColor: '#4D243D',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 8, padding:10, backgroundColor:'#409fff', 
  }
})
