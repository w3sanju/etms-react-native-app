import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet, FlatList, TouchableOpacity, BackHandler, Alert } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph, Divider, Dialog, Portal, Provider, Caption, ActivityIndicator, Colors, ProgressBar, Snackbar } from 'react-native-paper';

import styles from './DialogBoxStyle'


const rows = [
  { id: 0, title: 'View' },
  { id: 1, title: 'Text' },
  { id: 2, title: 'Image' },
  { id: 3, title: 'ScrollView' },
  { id: 4, title: 'ListView' },
]


function Item({ id, title, selected, onSelect }) {
  return (
    // <TouchableOpacity
    //   onPress={() => onSelect(id)}
    //    style={styles.listItems}
    // >
    //   <Text style={styles.title}>{title}</Text>
    // </TouchableOpacity>
    <Text style={styles.title}>{title}</Text>
  );
}

﻿export default class DialogBox extends React.Component {


  state = {
    visible: false,
    isLoading: true,
  };

  showDialog = () => this.setState({ visible: true });

  hideDialog = () => this.setState({ visible: false });

  render() {
    // const { isModalVisible } = this.props;



      // <Button style={{fontSize: 21,
      // backgroundColor:'skyblue',
      // marginTop: 32,
      // width: '100%', color:'#fff',
      // justifyContent: 'center',
      // alignItems: 'center',
      // height: 44}} title={'Pickup'} onPress={this.showDialog} />
      //
      // <Button color={'#000'} style={styles.dialogButton} onPress={this.showDialog}><Text style={{flex:1, textTransform: 'capitalize',}}>showDialog</Text></Button>



    return (
      // isModalVisible &&
        <Portal>
        <View>

        <Button style={{fontSize: 21,
        backgroundColor:'skyblue',
        marginTop: 32,
        width: '100%', color:'#fff',
        justifyContent: 'center',
        alignItems: 'center',
        height: 44}} title={'Pickup'} onPress={this.showDialog} />

          <Button style={styles.aligncenter, {marginTop: 150, backgroundColor: 'red'}} title={"Login page"} onPress={this.showDialog} />

        </View>
        <Dialog visible={this.state.visible}
               onDismiss={this.hideDialog} style={styles.dialogContainer}>
              <Dialog.Title style={styles.dialogTitle}>Check out Cab {this.props.greeting}</Dialog.Title>

                    <Button mode="text" color={'#fff'} onPress={this.hideDialog} style={styles.dialogCloseButton}><Text style={{fontSize:18, fontFamily: 'sans-serif-thin'}}>X</Text></Button>
              <Dialog.ScrollArea style={{  marginHorizontal:-25, }}>
                <ScrollView>
                  <Image style={styles.dialogCoverImage} source={{ uri: 'https://picsum.photos/500/800' }}/>

                    <FlatList style={styles.dialogFlatlist}
                        data={this.props.greetingArray}
                        renderItem={({ item, index }) => (
                          <View style={{ flex:1, paddingVertical:5, }}
                            id={item.id}
                          >
                          <Text style={styles.title,{color:'#000'}}>{index+1})
                          <Text style={styles.title,{color:'#409fff'}}> {item.username} </Text></Text></View>
                        )}
                        numColumns={2}
                        keyExtractor={item => item.id}
                      />
                      <View style={{marginHorizontal:20, marginBottom:20 }}>
                        <Caption style={styles.dialogCaption}>Cab No:</Caption>
                        <View style={styles.dialogParagraph}>
                          <Text style={styles.dialogLabel}>Vehicle Number : KL07</Text>
                          <Text style={styles.dialogLabel}>Vehicle Details : Lorem</Text>
                          <Text style={styles.dialogLabel}>{this.props.greetingArray}</Text>
                        </View>

                      </View>
                  </ScrollView>
              </Dialog.ScrollArea>
              <Dialog.Actions>
                <Button color={'#000'} style={styles.dialogButton} onPress={this.hideDialog}><Text style={{flex:1, textTransform: 'capitalize',}}>Close</Text></Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
    );
  }
}
