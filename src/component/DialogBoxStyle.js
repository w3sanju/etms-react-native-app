import React, { Component } from 'react'
import { Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const styles = {
  dialogContainer:{
    borderTopRightRadius: 10, borderTopLeftRadius: 10,  margin:0,
    overflow: 'hidden',
    paddingVertical:0,
    // flex:1,
    // flexDirection: 'column',
  },
  dialogCloseButton:{
    // flex:1,
    position:'absolute', right:-10, top:0, borderTopRightRadius: 15, padding:0,
  },
  dialogCaption:{fontSize:12, color:'#4861fb', fontWeight:'bold'},
  dialogTitle:{  backgroundColor:'#6200ee', paddingVertical:8, color:'white', fontSize:19,  textAlign:'center' , marginHorizontal:0, marginTop:0, marginBottom:0,
                  height:45, width:'100%',
                  fontFamily: 'sans-serif-thin',
                  fontWeight:'100' },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    fontSize: 17,
    marginHorizontal: 0,
    marginBottom: 20,
    paddingHorizontal: 0,
    // backgroundColor: '#4D243D'
    // fontWeight: 'bold',
    // color: COLOR.WHITE
  },
  dialogCoverImage:{width:'100%', height:100},
  dialogFlatlist:{flex: 1, height:'100%', margin:0,paddingHorizontal:20, marginHorizontal:0, marginTop:5, paddingVertical:15},
  dialogParagraph:{borderWidth: 0, borderLeftWidth: 4, marginHorizontal:0, padding:10, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderColor:'#6200ee', backgroundColor:'#f2f2f2', fontSize:12, paddingLeft:15},
  dialogButton:{flex:1, },
  dialogLabel:{fontSize: 12,
    marginHorizontal: 0,
    paddingVertical: 2,  }
}
export default styles;
﻿
