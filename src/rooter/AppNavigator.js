import { createAppContainer } from '@react-navigation/native';
import { createStackNavigator } from 'react-navigation-stack';
// import Login from '../screens/Login'
import Home from '../screens/Home/Home'
import Pickup from "../screens/Pickup/Pickup";

const AppNavigator = createStackNavigator({

    // Login: {
    //     screen: Login,
    //     navigationOptions: ({navigation}) => ({
    //         header: null,
    //     })
    // },
    Home:{
        screen:Home,
        navigationOptions: ({navigation}) => ({
            header: null,
            gesturesEnabled: false,
        })
    },
    Pickup:{
        screen:Pickup,
        navigationOptions: ({navigation}) => ({
            header: null,
            gesturesEnabled: false,
        })
    }
    // Detail:{
    //     screen:Detail,
    //     navigationOptions: ({navigation}) => ({
    //         title: 'Detail Page',
    //         headerStyle: {
    //             backgroundColor: '#f4511e',
    //         },
    //         headerTintColor: '#fff',
    //         headerTitleStyle: {
    //             fontWeight: 'bold',
    //         },
    //     })
    // }

},
{
  initialRouteName:'Home',
  mode: 'modal'
});
export default createAppContainer(AppNavigator);
