import React, { Component } from 'react';
import {View, Text, Image, ImageBackground, SafeAreaView,ScrollView, StyleSheet, FlatList, TouchableOpacity, BackHandler, Alert, Button } from 'react-native';
import { Avatar, Card, Title, Paragraph, Divider, Dialog, Portal, Provider, Caption, ActivityIndicator, Colors, ProgressBar, Snackbar } from 'react-native-paper';

﻿export default class MyPopup extends React.Component {

  state = {
    visible: false,
    isLoading: true,
    snackbarVisible: false
  };

  showDialog = () => this.setState({ visible: true });

  hideDialog = () => this.setState({ visible: false });

  render() {
    // const { isModalVisible } = this.props;

    return (
      // isModalVisible &&
        <Portal>
        <View>
          <Button color="#E91E63" title={'Show Dialog'} onPress={this.showDialog} />
        </View>
        <Dialog visible={this.state.visible}
               onDismiss={this.hideDialog}>
              <Dialog.Title>Check out Cab</Dialog.Title>
                    <Button mode="text" color={'#000'} onPress={this.hideDialog}><Text style={{fontSize:18, fontFamily: 'sans-serif-thin'}}>X</Text></Button>
              <Dialog.ScrollArea style={{  marginHorizontal:-25, }}>
                <ScrollView>
                  <Image source={{ uri: 'https://picsum.photos/500/800' }}/>
                    <View style={{backgroundColor:'#0f0'}}>
                      <Text>Hi lorem</Text>
                    </View>
                  </ScrollView>
              </Dialog.ScrollArea>
              <Dialog.Actions>
                <Button color={'#000'} onPress={this.hideDialog}><Text style={{flex:1, textTransform: 'capitalize',}}>Close</Text></Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
    );
  }
}
