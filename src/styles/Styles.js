/*
 * Copyright (c) 2011-2018, Zingaya, Inc. All rights reserved.
 */

'use strict';

import { StyleSheet, Platform } from 'react-native';
import COLOR from './Color';
import fonts from './fonts';

export default StyleSheet.create({
    safearea: {
        // flex: 1,
        // backgroundColor: COLOR.WHITE,
    },
    aligncenter: {
        // flexDirection: 'column',
        // justifyContent: 'center',
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    containerAbsolute:{
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    },
    modalBackground: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        padding: 20,
    },
    innerContainer: {
        borderRadius: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    innerContainerTransparent: {
        backgroundColor: COLOR.WHITE,
        padding: 20,
    },
    appheader: {
        resizeMode: 'contain',
        height: 60,
        alignSelf: 'center',
    },

    loginbutton: {
        color: COLOR.BUTTON,
        fontSize: 16,
        alignSelf: 'center',
        paddingTop: 20,
        textAlign: 'center',
    },
    forminput: {
        padding: 5,
        marginBottom: 10,
        color: COLOR.ACCENT,
        height: 40,
        borderColor: COLOR.ACCENT,
        borderWidth: 1,
        borderRadius: 4,
    },
    useragent: {
        flex: 1,
        flexDirection: 'column',
    },
    selfview: {
        position: 'absolute',
        right: 20,
        bottom: 20,
        width: 100,
        height: 120,
    },
    remotevideo: {
        flex: 1,
    },
    videoPanel: {
        flex: 1,
        position: 'relative',
    },
    call_controls: {
        height: 70,
    },
    margin: {
        margin: 10,
    },
    paddingY: {
        paddingHorizontal: 20
    },
    call_connecting_label: {
        fontSize: 18,
        alignSelf: 'center',
    },
    headerButton: {
        color: COLOR.WHITE,
        fontSize: 16,
        alignSelf: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
        textAlign: 'center',
    },
    incoming_call: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 22,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    loginForm: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        margin:20,
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
    },
    loginContainer:{
        backgroundColor: COLOR.WHITE,
        width:'80%',
        height:'50%',
        textAlign: 'center',
        alignItems: 'center',
    },
    title: {
      fontSize: 19,
      fontWeight: 'bold',
      // color: COLOR.WHITE
    },
      h1: {
        color: COLOR.BLACK,
        fontSize: 34,
        ...Platform.select({
          ios: {
            fontFamily: fonts.primary,
            fontWeight: 'bold',
          },
          android: {
            fontFamily: fonts.primaryBold,
          },
        }),
      },
    scrollView: {}
});
